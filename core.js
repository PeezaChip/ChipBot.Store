const path = require("path");
const fs = require("fs");

const dir = path.join(__dirname, "public");

function getPaths(reqPath) {
    reqPath = reqPath.substr(1);
    let split = reqPath.split("/");

    let systemPath = path.join(dir, ...split);
    let tarPath = `${systemPath}.tgz`;
    let md5Path = `${systemPath}.md5`;

    return {
        tarPath: tarPath,
        md5Path: md5Path
    };
}

function exists(paths) {
    if (!fs.existsSync(paths.tarPath)) return false;
    if (!fs.existsSync(paths.md5Path)) return false;
    return true;
}

module.exports = {
    dir: dir,
    getPaths: getPaths,
    exists: exists
}