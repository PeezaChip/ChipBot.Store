const express = require("express");
const fs = require("fs");
const core = require("./core");

const app = express();

function log(text) { console.log(`[${new Date().toLocaleString()}] ${text}`); }

function processRequest(req, res) {
    let paths = core.getPaths(req.path);

    try {
        if (!core.exists(paths)) throw new Error("tar or md5 was not found");
    } catch (e) {
        log(`404: ${e.message} for ${req.path}`);
        return { code: 404 };
    }

    let md5 = fs.readFileSync(paths.md5Path);
    res.setHeader("X-Chip-Plugin-MD5", md5.toString().trim());
    return { file: paths.tarPath };
}

if (!fs.existsSync(core.dir)) fs.mkdirSync(core.dir);

process.title = "ChipBot.Store";

app.get("*", (req, res) => {
    let result = {};

    try {
        result = processRequest(req, res);
    } catch (e) {
        log(`coudn't process request: ${e.message}`);
    }

    if (result.file) {
        log(`sending tar for: ${req.path}`);
        res.sendFile(result.file);
    } else {
        res.sendStatus(result.code || 500);
    }
});

let port = process.argv[2] || 8085;
app.listen(port, () => { console.log(`Server is listening on ${port}`); });