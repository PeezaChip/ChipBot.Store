# ChipBot.Store

Simple repository that can supply bot plugins

## Requirments

Run `npm install` to get the dependencies.

## Running

Simply `node app.js` by default it will launch on port 8085, you can specify different port by doing so `node app.js 8090`.

## How it works

* On launch it will create `public` directory if it doesn't exist already.
* It will take request path and convert it to system path relative to `public` directory.
* If either of `.tgz` or `.md5` files with that name are missing server will respond with `404`.
* Otherwise it will set http header `X-Chip-Plugin-MD5` to contents of `.md5` file.
* And send `.tgz` file.

## Example

* User requests `http://server.com/path/file`.
* Server will search for `./public/path/file.tgz` and `./public/path/file.md5`
* If at least one of them is missing server will respond `404`.